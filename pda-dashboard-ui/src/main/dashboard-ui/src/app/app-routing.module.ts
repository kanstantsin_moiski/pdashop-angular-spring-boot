import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  { path: '', loadChildren: 'app/core/core.module#CoreModule' }
];

export const routing = RouterModule.forRoot(ROUTES);
