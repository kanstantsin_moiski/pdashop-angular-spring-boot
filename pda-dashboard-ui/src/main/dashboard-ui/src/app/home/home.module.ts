import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { HomeComponent } from "./home.component";
import { RouterModule } from "@angular/router";

import { CarouselModule } from "ngx-bootstrap";

const HOME_ROUTE = [
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CarouselModule.forRoot(),
    RouterModule.forChild(HOME_ROUTE)
  ],
  declarations: [
    HomeComponent
  ]
})
export class HomeModule {}
