import { HttpClient, HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { catchError, map } from "rxjs/operators";
import { _throw } from "rxjs/observable/throw";
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";
import { Category } from "../../readers/models/category.model";

@Injectable()
export class CategoryObservableService {

  public apiURLRest: string = 'api/v1';

  constructor(
    private http: HttpClient
  ){ }

  getCategories(): Observable<Category[]> {
    return this.http
      .get(`${this.apiURLRest}/categories`)
      .pipe(map(this.handleData), catchError(this.handleError));
  }

  private handleData(response: HttpResponse<Category>){
    const body = response;
    return body || {};
  }

  private handleError(err: HttpErrorResponse){
    let errorMessage: string;
    if (err.error instanceof Error){
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Error: ${
        err.error.description
        }`;
    }
    console.error(errorMessage);
    return _throw(errorMessage);
  }

}
