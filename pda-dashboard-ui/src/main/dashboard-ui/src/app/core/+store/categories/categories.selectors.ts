import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CategoriesState } from "./categories.state";

const getEntities = (state: CategoriesState) => state.entities;
const getOriginalCategory = (state: CategoriesState) => state.originalCategory;
const getLoaded = (state: CategoriesState) => state.loaded;
const getLoading = (state: CategoriesState) => state.loading;
const getError = (state: CategoriesState) => state.error;

export const getCategoryState = createFeatureSelector<CategoriesState>('categories');

const getCategoriesEntities = createSelector(getCategoryState, getEntities);
export const getCategoriesOriginalReader = createSelector(getCategoryState, getOriginalCategory);
export const getCategoriesLoaded = createSelector(getCategoryState, getLoaded);
export const getCategoriesLoading = createSelector(getCategoryState, getLoading);
export const getCategoriesError = createSelector(getCategoryState, getError);

export const getCategories = createSelector(getCategoriesEntities, entities => {
  return Object.keys(entities).map(id => entities[+id]);
});
