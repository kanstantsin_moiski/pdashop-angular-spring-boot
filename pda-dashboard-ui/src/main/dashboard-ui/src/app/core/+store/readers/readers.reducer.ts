import { initionalReadersState, ReadersState } from "./readers.state";
import { ReadersActions, ReadersActionTypes } from "./readers.action";

import { Reader } from "../../../readers/models/reader.model";

export function readersReducer (state = initionalReadersState, action: ReadersActions): ReadersState {

  switch (action.type){

    case ReadersActionTypes.GET_READERS: {
      return {
        ...state,
        loading: true
      };
    }

    case ReadersActionTypes.GET_READERS_SUCCESS: {
      const readers = <Reader[]>action.payload;
      console.log(readers);
      const entities = readers.reduce(
        (result: {[id: number]: Reader}, reader: Reader) => {
          return {
            ...result,
            [reader.id]: reader
          };
        },
        {
          ...state.entities
        }
      );
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      };
    }

    case ReadersActionTypes.GET_READERS_ERROR:
    case ReadersActionTypes.CREATE_READER_ERROR:
    case ReadersActionTypes.DELETE_READER_ERROR: {
      const error = action.payload;
      return {
        ...state,
        loading: false,
        loaded: false,
        error
      };
    }

    case ReadersActionTypes.DELETE_READER_SUCCESS: {
      const reader = <Reader>action.payload;
      const { [reader.id]: removed, ...entities } = state.entities;

      return {
        ...state,
        entities
      };
    }

    case ReadersActionTypes.CREATE_READER_SUCCESS: {
      const reader = <Reader>action.payload;
      const entities = {
        ...state.entities,
        [reader.id]: reader
      };
      const originalReader = {...<Reader>action.payload};
      return {
        ...state,
        entities,
        originalReader
      };
    }

    case ReadersActionTypes.DELETE_READER:
    case ReadersActionTypes.CREATE_READER: {
      return {
        ...state
      };
    }

    default: {
      return state;
    }
  }

}
