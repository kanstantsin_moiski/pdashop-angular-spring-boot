import { Action } from "@ngrx/store";
import { Reader } from "../../../readers/models/reader.model";

export enum ReadersActionTypes {
  GET_READERS             = '[Readers] GET_READERS',
  GET_READERS_SUCCESS     = '[Readers] GET_READERS_SUCCESS',
  GET_READERS_ERROR       = '[Readers] GET_READERS_ERROR',
  DELETE_READER           = '[Readers] DELETE_READER',
  DELETE_READER_SUCCESS   = '[Readers] DELETE_READER_SUCCESS',
  DELETE_READER_ERROR     = '[Readers] DELETE_READER_ERROR',
  CREATE_READER           = '[Readers] CREATE_READER',
  CREATE_READER_SUCCESS   = '[Readers] CREATE_READER_SUCCESS',
  CREATE_READER_ERROR     = '[Readers] CREATE_READER_ERROR'
}

export class GetReaders implements Action {
  readonly type = ReadersActionTypes.GET_READERS;
}

export class GetReadersSuccess implements Action {
  readonly type = ReadersActionTypes.GET_READERS_SUCCESS;
  constructor ( public payload: Reader[] ){ }
}

export class GetReadersError implements Action {
  readonly type = ReadersActionTypes.GET_READERS_ERROR;
  constructor ( public payload: Error | string ) { }
}

export class DeleteReader implements Action {
  readonly type = ReadersActionTypes.DELETE_READER;
  constructor ( public payload: Reader) { }
}

export class DeleteReaderSuccess implements Action {
  readonly type = ReadersActionTypes.DELETE_READER_SUCCESS;
  constructor ( public payload: Reader) { }
}

export class DeleteReaderError implements Action {
  readonly type = ReadersActionTypes.DELETE_READER_ERROR;
  constructor ( public payload: Error | string) { }
}

export class CreateReader implements Action {
  readonly type = ReadersActionTypes.CREATE_READER;
  constructor(public payload: Reader) {}
}

export class CreateReaderSuccess implements Action {
  readonly type = ReadersActionTypes.CREATE_READER_SUCCESS;
  constructor(public payload: Reader) { }
}

export class CreateReaderError implements Action {
  readonly type = ReadersActionTypes.CREATE_READER_ERROR;
  constructor(public payload: Error | string) {}
}

export type ReadersActions =
    GetReaders
  | GetReadersSuccess
  | GetReadersError
  | DeleteReader
  | DeleteReaderSuccess
  | DeleteReaderError
  | CreateReader
  | CreateReaderSuccess
  | CreateReaderError;
