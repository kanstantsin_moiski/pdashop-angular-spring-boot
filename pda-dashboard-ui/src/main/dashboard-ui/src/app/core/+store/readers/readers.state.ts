import {Reader} from "../../../readers/models/reader.model";

export interface ReadersState {
  entities: Readonly<{ [id: number]: Reader }>;
  originalReader: Readonly<Reader>;
  readonly loading: boolean;
  readonly loaded: boolean;
  readonly error: Error | string;
}

export const initionalReadersState: ReadersState = {
  entities: {},
  originalReader: null,
  loading: false,
  loaded: false,
  error: null
}
