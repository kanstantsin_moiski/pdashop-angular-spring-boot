import { Action } from "@ngrx/store";
import { catchError, map, switchMap } from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { Actions, Effect, ofType } from "@ngrx/effects";
import * as CategoriesActions from "../categories/categories.action";
import { Injectable } from "@angular/core";
import { of } from "rxjs/observable/of";
import { CategoryObservableService } from "../../services/category-observable.service";

@Injectable()
export class CategoriesEffects {
  constructor(
    private actions$: Actions,
    private categoryObservableService: CategoryObservableService
  ){ }

  @Effect()
  getCategories$: Observable<Action> = this.actions$.pipe(
    ofType<CategoriesActions.GetCategories>(CategoriesActions.CategoriesActionTypes.GET_CATEGORIES),
    switchMap( action =>
      this.categoryObservableService
        .getCategories()
        .pipe(
          map( categories => new CategoriesActions.GetCategoriesSuccess(categories)),
          catchError(err => of(new CategoriesActions.GetCategoriesError(err)))
        )
    )
  );

}
