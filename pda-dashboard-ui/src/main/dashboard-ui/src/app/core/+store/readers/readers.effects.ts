import { Injectable } from "@angular/core";

import { Actions, Effect, ofType} from "@ngrx/effects";
import { ReaderObservableService } from "../../../readers/services/reader-observable.service";
import { Observable } from "rxjs/Observable";
import { Action } from "@ngrx/store";

import * as ReadersActions from './readers.action';
import { catchError, map, pluck, switchMap, concatMap } from "rxjs/operators";
import { of } from "rxjs/observable/of";

import { Reader } from "../../../readers/models/reader.model";

@Injectable()
export class ReadersEffects {
  constructor(
    private actions$: Actions,
    private readerObservableService: ReaderObservableService
  ){
    console.log('[READERS EFFECTS]');
  }

  @Effect()
  getReaders$: Observable<Action> = this.actions$.pipe(
    ofType<ReadersActions.GetReaders>(ReadersActions.ReadersActionTypes.GET_READERS),
    switchMap( action =>
      this.readerObservableService
        .getReaders()
        .pipe(
          map( readers => new ReadersActions.GetReadersSuccess(readers)),
          catchError(err => of(new ReadersActions.GetReadersError(err)))
        )
    )
  );

  @Effect()
  deleteReader$: Observable<Action> = this.actions$.pipe(
    ofType<ReadersActions.DeleteReader>(ReadersActions.ReadersActionTypes.DELETE_READER),
    pluck('payload'),
    concatMap((payload: Reader) =>
      this.readerObservableService.deleteReader(payload).pipe(
        map(() => new ReadersActions.DeleteReaderSuccess(payload)),
        catchError( err => of(new ReadersActions.DeleteReaderError(err)))
      )
    )
  );

  @Effect()
  createReader$: Observable<Action> = this.actions$.pipe(
    ofType<ReadersActions.CreateReader>(ReadersActions.ReadersActionTypes.CREATE_READER),
    pluck('payload'),
    concatMap((payload: Reader) =>
      this.readerObservableService.createReader(payload).pipe(
        map(reader => new ReadersActions.CreateReaderSuccess(reader)),
        catchError(err => of(new ReadersActions.CreateReaderError(err)))
      )
    )
  );

}
