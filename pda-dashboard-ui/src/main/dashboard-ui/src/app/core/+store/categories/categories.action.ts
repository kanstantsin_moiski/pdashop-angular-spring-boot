import { Action } from "@ngrx/store";
import { Category } from "../../../readers/models/category.model";

export enum CategoriesActionTypes {
  GET_CATEGORIES             = '[Categories] GET_CATEGORIES',
  GET_CATEGORIES_SUCCESS     = '[Categories] GET_CATEGORIES_SUCCESS',
  GET_CATEGORIES_ERROR       = '[Categories] GET_CATEGORIES_ERROR',
}

export class GetCategories implements Action {
  readonly type = CategoriesActionTypes.GET_CATEGORIES;
}

export class GetCategoriesSuccess implements Action {
  readonly type = CategoriesActionTypes.GET_CATEGORIES_SUCCESS;
  constructor ( public payload: Category[] ){ }
}

export class GetCategoriesError implements Action {
  readonly type = CategoriesActionTypes.GET_CATEGORIES_ERROR;
  constructor ( public payload: Error | string ) { }
}

export type CategoriesActions =
  GetCategories
  | GetCategoriesSuccess
  | GetCategoriesError;
