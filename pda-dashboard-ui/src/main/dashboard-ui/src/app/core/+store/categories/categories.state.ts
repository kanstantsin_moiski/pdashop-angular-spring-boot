import { Category } from "../../../readers/models/category.model";

export interface CategoriesState {
  entities: Readonly<{ [id: number]: Category }>;
  originalCategory: Readonly<Category>;
  readonly loading: boolean;
  readonly loaded: boolean;
  readonly error: Error | string;
}

export const initionalCategoriesState: CategoriesState = {
  entities: {},
  originalCategory: null,
  loading: false,
  loaded: false,
  error: null
}
