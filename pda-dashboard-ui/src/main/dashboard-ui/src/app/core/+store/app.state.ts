import { ReadersState } from "./readers";
import { CategoriesState } from "./categories/categories.state";

export interface AppState {
  readers: ReadersState;
  categories: CategoriesState;
}
