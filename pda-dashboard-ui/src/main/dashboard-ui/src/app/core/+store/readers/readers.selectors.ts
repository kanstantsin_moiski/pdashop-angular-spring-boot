import { ReadersState } from "./readers.state";
import { createFeatureSelector, createSelector } from "@ngrx/store";

const getEntities = (state: ReadersState) => state.entities;
const getOriginalReader = (state: ReadersState) => state.originalReader;
const getLoaded = (state: ReadersState) => state.loaded;
const getLoading = (state: ReadersState) => state.loading;
const getError = (state: ReadersState) => state.error;

export const getReaderState = createFeatureSelector<ReadersState>('readers');

const getReadersEntities = createSelector(getReaderState, getEntities);
export const getReadersOriginalReader = createSelector(getReaderState, getOriginalReader);
export const getReadersLoaded = createSelector(getReaderState, getLoaded);
export const getReadersLoading = createSelector(getReaderState, getLoading);
export const getReadersError = createSelector(getReaderState, getError);

export const getReaders = createSelector(getReadersEntities, entities => {
  return Object.keys(entities).map(id => entities[+id]);
});
