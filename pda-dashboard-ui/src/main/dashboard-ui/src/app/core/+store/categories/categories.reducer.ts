import { CategoriesState, initionalCategoriesState } from "./categories.state";
import { CategoriesActions, CategoriesActionTypes } from "./categories.action";
import { Category } from "../../../readers/models/category.model";

export function categoriesReducer (state = initionalCategoriesState, action: CategoriesActions): CategoriesState {

  switch (action.type){

    case CategoriesActionTypes.GET_CATEGORIES: {
      return {
        ...state,
        loading: true
      };
    }

    case CategoriesActionTypes.GET_CATEGORIES_SUCCESS: {
      const categories = <Category[]>action.payload;
      console.log(categories);
      const entities = categories.reduce(
        (result: {[id: number]: Category}, category: Category) => {
          return {
            ...result,
            [category.id]: category
          };
        },
        {
          ...state.entities
        }
      );
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      };
    }

    case CategoriesActionTypes.GET_CATEGORIES_ERROR: {
      const error = action.payload;
      return {
        ...state,
        loading: false,
        loaded: false,
        error
      };
    }

    default: {
      return state;
    }
  }

}
