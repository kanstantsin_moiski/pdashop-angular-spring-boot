import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { CoreRouting } from "./core.routing";
import { CoreComponent } from "./core.component";
import { HeaderComponent } from "./components/header/header.component";
import { NavigationComponent } from "./components/navigation/navigation.component";
import { PathNotFoundComponent } from './components/path-not-found/path-not-found.component';
import { CoreStoreModule } from "./+store/core-store.module";
import { CategoryObservableService } from "./services/category-observable.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CoreRouting,
    CoreStoreModule
  ],
  declarations: [
    CoreComponent,
    HeaderComponent,
    NavigationComponent,
    PathNotFoundComponent
  ],
  providers: [
    CategoryObservableService
  ]
})
export class CoreModule { }
