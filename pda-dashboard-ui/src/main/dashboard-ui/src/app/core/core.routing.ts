import { RouterModule, Routes } from '@angular/router';
import { CoreComponent } from './core.component';;
import { PathNotFoundComponent } from "./components/path-not-found/path-not-found.component";

const CORE_ROUTES: Routes = [{
  path: '',
  component: CoreComponent,
  children: [
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full'
    },
    {
      path: 'home',
      loadChildren: '../home/home.module#HomeModule'
    },
    {
      path: 'readers',
      loadChildren: '../readers/readers.module#ReadersModule',
      data: {
        preload: true,
        title: 'E-Readers'
      }
    },
    {
      path: '**',
      component: PathNotFoundComponent,
      data: {
        title: 'Page Not Found'
      }
    }
  ]
}];

export const CoreRouting = RouterModule.forChild(CORE_ROUTES);
