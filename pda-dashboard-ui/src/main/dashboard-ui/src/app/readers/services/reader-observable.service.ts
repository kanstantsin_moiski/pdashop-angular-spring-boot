import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from "@angular/common/http";
import { Injectable} from "@angular/core";

import { Reader } from "../models/reader.model";

import { Observable } from "rxjs/Observable";
import { catchError, concatMap, map } from "rxjs/operators";
import { _throw } from "rxjs/observable/throw";

@Injectable()
export class ReaderObservableService {

  public apiURLRest: string = 'api/v1';

  constructor(
    private http: HttpClient
  ){ }

  getReaders(): Observable<Reader[]> {
    return this.http
      .get(`${this.apiURLRest}/readers`)
      .pipe(map(this.handleData), catchError(this.handleError));
  }

  deleteReader(reader: Reader): Observable<Reader[]>{
    return this.http
      .delete(`${this.apiURLRest}/readers/${reader.id}`)
      .pipe(
        concatMap(() => this.getReaders())
      );
  }

  createReader(reader: Reader): Observable<Reader> {
    const url = `${this.apiURLRest}/readers`,
      body = JSON.stringify(reader),
      options = {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json'),
      };
    return this.http
      .post(url, body, options)
      .pipe(map(this.handleData), catchError(this.handleError));
  }

  private handleData(response: HttpResponse<Reader>){
    const body = response;
    return body || {};
  }

  private handleError(err: HttpErrorResponse){
    let errorMessage: string;
    if (err.error instanceof Error){
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Error: ${
        err.error.description
      }`;
    }
    console.error(errorMessage);
    return _throw(errorMessage);
  }

}
