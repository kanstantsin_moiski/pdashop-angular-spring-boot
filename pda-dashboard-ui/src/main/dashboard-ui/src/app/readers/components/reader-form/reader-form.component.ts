import { Component, OnDestroy, OnInit } from '@angular/core';
import { Category } from "../../models/category.model";
import { FormBuilder , FormGroup, Validators } from "@angular/forms";
import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";
import { AppState, getCategories, getCategoriesError } from "../../../core/+store";
import { select, Store } from "@ngrx/store";
import { Location } from '@angular/common';

import * as CategoriesActions from './../../../core/+store/categories/categories.action';
import * as ReadersActions from './../../../core/+store/readers/readers.action';

import { Reader } from "../../models/reader.model";

@Component({
  selector: 'app-reader-form',
  templateUrl: './reader-form.component.html',
  styleUrls: ['./reader-form.component.css']
})
export class ReaderFormComponent implements OnInit, OnDestroy {

  currentCategory: Category;
  category: Category;
  currentReader: Reader;
  categories: Category[];

  categories$: Observable<Array<Category>>;
  categoriesError$: Observable<Error | string>;

  readerForm: FormGroup;

  private sub: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.categories$ = this.store.pipe(select(getCategories));
    this.categoriesError$ = this.store.pipe(select(getCategoriesError));
    this.store.dispatch(new CategoriesActions.GetCategories());
    this.buildForm();
  }

  private buildForm(){
    this.readerForm = this.fb.group({
      categoryId: '',
      name: [
        {value: '',  disabled: false},
        [Validators.required, Validators.minLength(3), Validators.maxLength(255) ]],
      description: [
        {value: '',  disabled: false},
        [Validators.required, Validators.minLength(3), Validators.maxLength(255)]
      ]
    });
  }

  saveReader() {
    let { categoryId, name, description } = this.readerForm.value;
    this.currentCategory = new Category(categoryId, '' ,'', '');
    this.currentReader = new Reader(null, name, description, '', '', this.currentCategory);
    this.store.dispatch(new ReadersActions.CreateReader( this.currentReader));
    this.readerForm.reset();
    this.goBack();
  }

  goBack():void{
    this.location.back()
  }

  ngOnDestroy() {
    this.sub.forEach(sub => sub.unsubscribe());
  }
}
