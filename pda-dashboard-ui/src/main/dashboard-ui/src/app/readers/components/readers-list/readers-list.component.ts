import { Component, OnInit } from '@angular/core';
import { AutoUnsubscribe } from "../../../core/decorators";
import { Observable } from "rxjs/Observable";
import { Reader } from "../../models/reader.model";
import { Subscription } from "rxjs/Subscription";
import { select, Store } from "@ngrx/store";


import { AppState, getReaders, getReadersError } from './../../../core/+store';

import * as ReadersActions from './../../../core/+store/readers/readers.action';

import { Router } from '@angular/router';

@Component({
  templateUrl: './readers-list.component.html',
  styleUrls: ['./readers-list.component.css']
})
@AutoUnsubscribe('subscription')
export class ReadersListComponent implements OnInit {
  readers$: Observable<Array<Reader>>;
  readersError$: Observable<Error | string>;

  private subscription: Subscription;

  constructor(
    private store: Store<AppState>,
    private router: Router
  ) { }

  ngOnInit() {
    this.readers$ = this.store.pipe(select(getReaders));
    this.readersError$ = this.store.pipe(select(getReadersError));
    this.store.dispatch(new ReadersActions.GetReaders());
  }

  onDeleteReader(reader: Reader) {
    this.store.dispatch(new ReadersActions.DeleteReader(reader));
  }

  onCreateReader() {
    const link = ['/readers/add'];
    this.router.navigate(link);
  }

}
