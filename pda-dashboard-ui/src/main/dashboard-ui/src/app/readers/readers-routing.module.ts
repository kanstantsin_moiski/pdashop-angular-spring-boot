import { RouterModule, Routes } from "@angular/router";
import { ReadersComponent } from "./readers.component";
import { ReadersListComponent } from "./components/readers-list/readers-list.component";
import { ReaderFormComponent } from "./components/reader-form/reader-form.component";
import { NgModule } from "@angular/core";

const routes: Routes = [
  {
    path: '',
    component: ReadersComponent,
    children: [
      {
        path: '',
        component: ReadersListComponent
      },
      {
        path: 'add',
        component: ReaderFormComponent
      }
    ]
  }
];

export let readersRouterComponents = [ReadersComponent, ReadersListComponent, ReaderFormComponent];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReadersRoutingModule {
}
