export class Category {
  constructor(
    public id: string,
    public name: string,
    public createdAt: string,
    public lastModified: string
  ){ }
}
