import {Category} from "./category.model";

export class Reader {
  constructor (
    public id: number,
    public name: string,
    public description: string,
    public createdAt: string,
    public lastModified: string,
    public category: Category
  ){ }
}
