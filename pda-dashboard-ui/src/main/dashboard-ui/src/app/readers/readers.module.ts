import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { readersRouterComponents, ReadersRoutingModule } from "./readers-routing.module";
import { ReadersComponent } from "./readers.component";
import { ReaderObservableService } from "./services/reader-observable.service";
import { StoreModule } from "@ngrx/store";
import { ReadersEffects, readersReducer } from "../core/+store/readers";
import { EffectsModule } from "@ngrx/effects";
import { CategoriesEffects, categoriesReducer } from "../core/+store/categories";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ReadersRoutingModule,
    StoreModule.forFeature('readers', readersReducer),
    StoreModule.forFeature('categories', categoriesReducer),
    EffectsModule.forFeature([ReadersEffects, CategoriesEffects])
  ],
  declarations: [
    readersRouterComponents,
    ReadersComponent
  ],
  providers: [
    ReaderObservableService
  ]
})
export class ReadersModule {}
