package by.moiski.pda.dashboard.controller;

import by.moiski.pda.dashboard.model.Product;
import by.moiski.pda.dashboard.service.ReaderService;
import by.moiski.pda.dashboard.service.impl.ReaderServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/readers", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ReaderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReaderController.class);

    private final ReaderService readerService;

    @Autowired
    public ReaderController(ReaderServiceImpl productService) {
        this.readerService = productService;
    }

    @GetMapping
    public List<Product> readProducts(){
        LOGGER.info("Retrieve E-Readers");
        return readerService.readProducts()
                .stream()
                .sorted(Comparator.comparing(Product::getName))
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Product createProduct(@RequestBody Product product){
        return readerService.createProduct(product);
    }

    @DeleteMapping("/{productId}")
    public void deleteReader(@PathVariable Long productId){
        LOGGER.info("Delete reader");
        readerService.deleteProduct(productId);
    }
}
