package by.moiski.pda.dashboard.service;

import by.moiski.pda.dashboard.model.Product;

import java.util.List;

public interface ReaderService {

    List<Product> readProducts();

    void deleteProduct(Long productId);

    Product createProduct(Product product);

}
