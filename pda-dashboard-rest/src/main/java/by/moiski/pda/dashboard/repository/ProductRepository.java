package by.moiski.pda.dashboard.repository;

import by.moiski.pda.dashboard.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("select p from Product p left join p.category c on p.category.id = c.id where c.name=:name")
    List<Product> findProductsByCategoryName(@Param("name") String categoryName);

}
