package by.moiski.pda.dashboard.controller.advice;

import by.moiski.pda.dashboard.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice("by.moiski.pda.dashboard.controller")
public class PdaDashboardExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdaDashboardExceptionHandler.class);

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> handleException(Exception ex){
        LOGGER.error("Exception occurred:", ex);
        return new ResponseEntity<>(new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionResponse> handleResourceNotFoundException(ResourceNotFoundException ex){
        return new ResponseEntity<>(new ExceptionResponse(HttpStatus.NOT_FOUND, ex.getMessage()), HttpStatus.NOT_FOUND);
    }
}
