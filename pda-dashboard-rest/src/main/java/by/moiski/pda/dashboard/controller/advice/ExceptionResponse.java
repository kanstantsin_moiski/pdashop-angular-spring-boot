package by.moiski.pda.dashboard.controller.advice;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ExceptionResponse {

    private HttpStatus status;
    private String description;
    private List<String> errors;

    public ExceptionResponse(HttpStatus status, String description) {
        this.status = status;
        this.description = description;
    }

    public ExceptionResponse(HttpStatus status, String description, List<String> errors) {
        this.status = status;
        this.description = description;
        this.errors = errors;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}
