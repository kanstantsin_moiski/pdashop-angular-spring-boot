package by.moiski.pda.dashboard.service.validator;

import by.moiski.pda.dashboard.model.Product;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Objects;

@Component
public class ReaderValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReaderValidator.class);

    private final static int READER_NAME_MIN_LENGTH = 3;
    private final static int READER_NAME_MAX_LENGTH = 255;
    private final static int READER_DESCRIPTION_MIN_LENGTH = 3;
    private final static int READER_DESCRIPTION_MAX_LENGTH = 255;
    private final static String READER_CATEGORY_NAME = "E-Readers";

    public ReaderValidator() {
    }

    public void validate( Product product ) {
        LOGGER.debug( "Validate reader {}", product );
        Validate.isTrue(StringUtils.hasText( product.getName()), "Reader name is required");
        Validate.isTrue(StringUtils.hasText( product.getDescription()), "Reader description is required");
        Validate.isTrue(product.getName().length() >= READER_NAME_MIN_LENGTH, "Reader name is small");
        Validate.isTrue(product.getName().length() <= READER_NAME_MAX_LENGTH, "Reader name is too long");
        Validate.isTrue(product.getDescription().length() >= READER_DESCRIPTION_MIN_LENGTH, "Reader description is small");
        Validate.isTrue(product.getDescription().length() <= READER_DESCRIPTION_MAX_LENGTH, "Reader description is too long");
        Validate.isTrue(Objects.nonNull(product.getCategory()), "Category is required");
        Validate.isTrue(Objects.equals(product.getCategory().getName(), READER_CATEGORY_NAME), "Choose category name E-Reader");
    }
}
