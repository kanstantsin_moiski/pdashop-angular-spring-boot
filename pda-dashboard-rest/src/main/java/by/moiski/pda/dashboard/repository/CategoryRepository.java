package by.moiski.pda.dashboard.repository;

import by.moiski.pda.dashboard.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
