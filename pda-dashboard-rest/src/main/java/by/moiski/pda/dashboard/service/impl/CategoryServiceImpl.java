package by.moiski.pda.dashboard.service.impl;

import by.moiski.pda.dashboard.model.Category;
import by.moiski.pda.dashboard.repository.CategoryRepository;
import by.moiski.pda.dashboard.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryServiceImpl.class);

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> readCategories() {
        LOGGER.info("Start retrieving all categories from in-memory database");
        return categoryRepository.findAll();
    }
}
