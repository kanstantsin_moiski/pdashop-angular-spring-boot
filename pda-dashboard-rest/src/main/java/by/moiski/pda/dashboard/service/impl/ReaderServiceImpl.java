package by.moiski.pda.dashboard.service.impl;

import by.moiski.pda.dashboard.exception.ResourceNotFoundException;
import by.moiski.pda.dashboard.model.Category;
import by.moiski.pda.dashboard.model.Product;
import by.moiski.pda.dashboard.repository.CategoryRepository;
import by.moiski.pda.dashboard.repository.ProductRepository;
import by.moiski.pda.dashboard.service.ReaderService;
import by.moiski.pda.dashboard.service.validator.ReaderValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ReaderServiceImpl implements ReaderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReaderServiceImpl.class);

    private static final String READER_CATEGORY_NAME = "E-Readers";

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ReaderValidator readerValidator;

    @Autowired
    public ReaderServiceImpl(ProductRepository productRepository, CategoryRepository categoryRepository, ReaderValidator validator) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.readerValidator = validator;
    }

    @Override
    public List<Product> readProducts() {
        LOGGER.info("Start retrieving all E-Reader from in-memory database");
        return productRepository.findProductsByCategoryName(READER_CATEGORY_NAME);
    }

    @Override
    public void deleteProduct(Long productId) {
        LOGGER.info("Start deleting reader from in-memory database. Product id: {" + productId + "}");
        Product reader = retrieveReader(productId);
        productRepository.delete(reader);
    }

    @Override
    public Product createProduct(Product product) {
        LOGGER.info("Create new E-reader");
        Category category = retrieveCategory(product.getCategory().getId());
        product.setCategory(category);
        readerValidator.validate(product);
        return productRepository.save(product);
    }

    private Product retrieveReader(Long productId) {
        return productRepository.findById(productId).orElseThrow(
                () -> new ResourceNotFoundException(String.format("Reader with id=%s was not found", productId))
        );
    }

    private Category retrieveCategory(Long categoryId){
        return categoryRepository.findById(categoryId).orElseThrow(
                () -> new ResourceNotFoundException(String.format("Category with id=%s was not found", categoryId))
        );
    }
}
