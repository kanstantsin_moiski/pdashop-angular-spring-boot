package by.moiski.pda.dashboard.service;

import by.moiski.pda.dashboard.model.Category;

import java.util.List;

public interface CategoryService {

    List<Category> readCategories();

}
