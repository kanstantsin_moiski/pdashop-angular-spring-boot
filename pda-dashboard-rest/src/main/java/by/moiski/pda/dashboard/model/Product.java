package by.moiski.pda.dashboard.model;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product extends BaseEntity {

    private Long id;
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    public Long getId() {
        return id;
    }

    private String name;
    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    private String description;
    @Column(name = "description", nullable = false)
    public String getDescription() {
        return description;
    }

    private Category category;
    @OneToOne(cascade = CascadeType.REFRESH)
    public Category getCategory() {
        return category;
    }

    public Product() {
    }

    public Product(String name, String description, Category category) {
        this.name = name;
        this.description = description;
        this.category = category;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
