package by.moiski.pda.dashboard.controller;

import by.moiski.pda.dashboard.exception.ResourceNotFoundException;
import by.moiski.pda.dashboard.model.Category;
import by.moiski.pda.dashboard.model.Product;
import by.moiski.pda.dashboard.service.impl.ReaderServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ReaderController.class)
public class ReaderControllerTest {

    @MockBean
    private ReaderServiceImpl readerService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void readProductsShouldReturnProducts() throws Exception {
        List<Product> products = createProducts();
        given(readerService.readProducts()).willReturn(products);
        this.mockMvc.perform(get("/api/v1/readers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].createdAt").value("2018-05-01 10:10:11"))
                .andExpect(jsonPath("$[0].lastModified").value("2018-05-01 10:10:11"))
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").value("name_one"))
                .andExpect(jsonPath("$[0].description").value("description_one"))
                .andExpect(jsonPath("$[0].category.id").value(1L))
                .andExpect(jsonPath("$[0].category.name").value("name_test"))
                .andExpect(jsonPath("$[0].category.createdAt").value("2018-04-01 10:10:11"))
                .andExpect(jsonPath("$[0].category.lastModified").value("2018-04-01 10:10:11"))
                .andExpect(jsonPath("$[1].createdAt").value("2018-05-02 10:10:11"))
                .andExpect(jsonPath("$[1].lastModified").value("2018-05-02 10:10:11"))
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].name").value("name_two"))
                .andExpect(jsonPath("$[1].description").value("description_two"))
                .andExpect(jsonPath("$[1].category.id").value(1L))
                .andExpect(jsonPath("$[1].category.name").value("name_test"))
                .andExpect(jsonPath("$[1].category.createdAt").value("2018-04-01 10:10:11"))
                .andExpect(jsonPath("$[1].category.lastModified").value("2018-04-01 10:10:11"))
                .andExpect(jsonPath("$.*", hasSize(2)));
        verify(readerService, times(1)).readProducts();
    }

    @Test
    public void readProductShouldReturnEmptyResult() throws Exception {
        List<Product> products = new ArrayList<>();
        given(readerService.readProducts()).willReturn(products);
        this.mockMvc.perform(get("/api/v1/readers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().json("[]"))
                .andExpect(jsonPath("$.*", hasSize(0)));
        verify(readerService, times(1)).readProducts();
    }

    @Test
    public void deleteProductIsSuccessful() throws Exception {
        List<Product> products = createProducts();
        this.mockMvc.perform(delete("/api/v1/readers/" + products.get(0).getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteProductShouldReturnWrongMessage() throws Exception {
        Long productId = 999L;
        willThrow(new ResourceNotFoundException("Reader with id=" + productId + " was not found"))
                .given(readerService).deleteProduct(productId);
        this.mockMvc.perform(delete("/api/v1/readers/" + productId)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.description").value("Reader with id=" + productId + " was not found"));
    }

    private List<Product> createProducts(){
        Category category = new Category("name_test");
        category.setId(1L);
        category.setCreatedAt(LocalDateTime.of(2018, 04, 01, 10, 10, 11));
        category.setLastModified(LocalDateTime.of(2018, 04, 01, 10, 10, 11));

        Product productOne = new Product("name_one", "description_one", category);
        productOne.setId(1L);
        productOne.setCreatedAt(LocalDateTime.of(2018, 05, 01, 10, 10, 11));
        productOne.setLastModified(LocalDateTime.of(2018, 05, 01, 10, 10, 11));

        Product productTwo = new Product("name_two", "description_two", category);
        productTwo.setId(2L);
        productTwo.setCreatedAt(LocalDateTime.of(2018, 05, 02, 10, 10, 11));
        productTwo.setLastModified(LocalDateTime.of(2018, 05, 02, 10, 10, 11));

        List<Product> products = new ArrayList<>();
        products.add(productOne);
        products.add(productTwo);

        return products;
    }
}
