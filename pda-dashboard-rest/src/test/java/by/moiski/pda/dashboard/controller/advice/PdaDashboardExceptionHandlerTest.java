package by.moiski.pda.dashboard.controller.advice;

import by.moiski.pda.dashboard.exception.ResourceNotFoundException;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PdaDashboardExceptionHandlerTest {

    private PdaDashboardExceptionHandler handler = new PdaDashboardExceptionHandler();

    @Test
    public void handleException() {
        final Exception exception = new Exception("Test exception");
        final ResponseEntity<ExceptionResponse> responseEntity = handler.handleException(exception);
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getBody().getStatus());
        assertEquals("Test exception", responseEntity.getBody().getDescription());
    }

    @Test
    public void handleResourceNotFoundException() {
        final ResourceNotFoundException resourceNotFoundException = new ResourceNotFoundException("Resource does't find");
        final ResponseEntity<ExceptionResponse> responseEntity = handler.handleResourceNotFoundException(resourceNotFoundException);
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals("Resource does't find", responseEntity.getBody().getDescription());
    }
}
