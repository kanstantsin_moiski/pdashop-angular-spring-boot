package by.moiski.pda.dashboard.controller;

import by.moiski.pda.dashboard.model.Category;
import by.moiski.pda.dashboard.service.impl.CategoryServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
public class CategoryControllerTest {

    @MockBean
    private CategoryServiceImpl categoryService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void readCategoriesShouldReturnCategories() throws Exception {
        List<Category> categories = createCategories();
        given(categoryService.readCategories()).willReturn(categories);
        this.mockMvc.perform(get("/api/v1/categories"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$[0].createdAt").value("2018-04-01 10:10:11"))
                .andExpect(jsonPath("$[0].lastModified").value("2018-04-01 10:10:11"))
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").value("category_one"))
                .andExpect(jsonPath("$[1].createdAt").value("2018-04-01 10:10:11"))
                .andExpect(jsonPath("$[1].lastModified").value("2018-04-01 10:10:11"))
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].name").value("category_two"))
                .andExpect(jsonPath("$.*", hasSize(2)));
        verify(categoryService, times(1)).readCategories();
    }

    @Test
    public void readCategoriesShouldReturnEmptyResult() throws Exception {
        List<Category> categories = new ArrayList<>();
        given(categoryService.readCategories()).willReturn(categories);
        this.mockMvc.perform(get("/api/v1/categories"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().json("[]"))
                .andExpect(jsonPath("$.*", hasSize(0)));
        verify(categoryService, times(1)).readCategories();
    }

    private List<Category> createCategories(){
        Long categoryOneId = 1L;
        Long categoryTwoId = 2L;

        Category categoryOne = buildCategory(categoryOneId, "category_one");
        Category categoryTwo = buildCategory(categoryTwoId, "category_two");

        List<Category> result = new ArrayList<>();
        result.add(categoryOne);
        result.add(categoryTwo);
        return result;
    }

    private Category buildCategory(Long categoryId, String categoryName){
        Category result = new Category(categoryName);
        result.setCreatedAt(LocalDateTime.of(2018, 04, 01, 10, 10, 11));
        result.setLastModified(LocalDateTime.of(2018, 04, 01, 10, 10, 11));
        result.setId(categoryId);
        return result;
    }


}
