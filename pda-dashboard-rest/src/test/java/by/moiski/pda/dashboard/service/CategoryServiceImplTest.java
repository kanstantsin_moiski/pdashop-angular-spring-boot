package by.moiski.pda.dashboard.service;

import by.moiski.pda.dashboard.model.Category;
import by.moiski.pda.dashboard.repository.CategoryRepository;
import by.moiski.pda.dashboard.service.impl.CategoryServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CategoryServiceImplTest {

    private CategoryService categoryService;
    private CategoryRepository categoryRepositoryMock = Mockito.mock(CategoryRepository.class);

    @Before
    public void setup() {
        this.categoryService = new CategoryServiceImpl(categoryRepositoryMock);
    }

    @Test
    public void readCategoriesIsSuccessful() {
        List<Category> categories = createCategories();
        when(categoryRepositoryMock.findAll()).thenReturn(categories);
        List<Category> result = categoryService.readCategories();
        verify(categoryRepositoryMock).findAll();
        assertThat("Wrong size of result list", result, hasSize(2));
        assertThat("Unexpected items", result, hasItems(categories.get(0), categories.get(1)));
    }

    private List<Category> createCategories(){
        Long categoryOneId = 111L;
        Long categoryTwoId = 112L;

        Category categoryOne = buildCategory(categoryOneId, "category_one");
        Category categoryTwo = buildCategory(categoryTwoId, "category_two");

        List<Category> result = new ArrayList<>();
        result.add(categoryOne);
        result.add(categoryTwo);
        return result;
    }

    private Category buildCategory(Long categoryId, String categoryName){
        Category result = new Category(categoryName);
        result.setId(categoryId);
        return result;
    }
}
