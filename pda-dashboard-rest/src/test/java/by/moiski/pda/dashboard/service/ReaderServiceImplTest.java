package by.moiski.pda.dashboard.service;

import by.moiski.pda.dashboard.exception.ResourceNotFoundException;
import by.moiski.pda.dashboard.model.Category;
import by.moiski.pda.dashboard.model.Product;
import by.moiski.pda.dashboard.repository.CategoryRepository;
import by.moiski.pda.dashboard.repository.ProductRepository;
import by.moiski.pda.dashboard.service.impl.ReaderServiceImpl;
import by.moiski.pda.dashboard.service.validator.ReaderValidator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ReaderServiceImplTest {

    private final static String CATEGORY_NAME = "E-Readers";

    private ReaderService readerService;
    private ProductRepository productRepositoryMock = Mockito.mock(ProductRepository.class);
    private CategoryRepository categoryRepositoryMock = Mockito.mock(CategoryRepository.class);
    private ReaderValidator readerValidatorMock = Mockito.mock(ReaderValidator.class);

    @Before
    public void setup() {
        this.readerService = new ReaderServiceImpl(productRepositoryMock, categoryRepositoryMock, readerValidatorMock);
    }

    @Test
    public void readProductsIsSuccessful() {
        List<Product> products = createProducts();
        when(productRepositoryMock.findProductsByCategoryName(CATEGORY_NAME)).thenReturn(products);
        List<Product> result = readerService.readProducts();
        verify(productRepositoryMock).findProductsByCategoryName(CATEGORY_NAME);
        assertThat("Wrong size of result list", result, hasSize(2));
        assertThat("Unexpected items", result, hasItems(products.get(0), products.get(1)));
    }

    @Test
    public void deleteProductIsSuccessful(){
        Long productId = 123L;
        Long categoryId = 123L;
        Product reader = buildProduct(productId, "product_name",
                "description_name", categoryId, "category_name");
        when(productRepositoryMock.findById(reader.getId())).thenReturn(Optional.of(reader));
        readerService.deleteProduct(productId);
        verify(productRepositoryMock).findById(productId);
        verify(productRepositoryMock).delete(reader);
    }

    @Test
    public void deleteProductShouldReturnResourceNotFoundException(){
        Long productId = 123L;
        try {
            readerService.deleteProduct(productId);
            fail("Exception should be thrown!");
        } catch (ResourceNotFoundException ex){
            assertEquals("Reader with id=123 was not found", ex.getMessage());
        }
    }

    private List<Product> createProducts(){
        Long productOneId = 111L;
        Long productTwoId = 112L;
        Long categoryId = 111L;
        Product productOne = buildProduct(productOneId, "product_name_one",
                "description_name_one", categoryId, "category_name");
        Product productTwo = buildProduct(productTwoId, "product_name_two",
                "description_name_two", categoryId, "category_name");
        List<Product> result = new ArrayList<>();
        result.add(productOne);
        result.add(productTwo);
        return result;
    }

    private Product buildProduct(Long productId, String productName, String productDescription, Long categoryId, String categoryName){
        Category category = buildCategory(categoryId, categoryName);
        Product result = new Product(productName, productDescription, category);
        result.setId(productId);
        return result;
    }

    private Category buildCategory(Long categoryId, String categoryName){
        Category result = new Category(categoryName);
        result.setId(categoryId);
        return result;
    }

}
